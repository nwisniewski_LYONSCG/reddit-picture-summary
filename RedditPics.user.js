// ==UserScript==
// @name         New Userscript
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.reddit.com/r/pics/
// @match        http://www.reddit.com/r/pics/
// @grant        GM_addStyle
// @run-at       document-end
// @noframes
// ==/UserScript==

GM_addStyle("#summary:hover #summaryImg{ transform: scale(10) translateY(15px) translateX(17px); transition-duration: 1s;}");

(function() {
    'use strict';

    // Your code here...


    var imgs = document.getElementsByTagName('img');
    var div = document.createElement('div');
    var imdiv;
    var a;
    var imgArray = [];
    var img;
    var i;
    var bigDiv;
    var elements;
    div.style.backgroundColor = '#ccddff';
    div.style.height = "60px";
    div.id = "sumDiv";
    div.style.transition = "all .5s ease-in-out";


    for (i = 0; i < imgs.length; i++){
        img = document.createElement('img');
        a = document.createElement('a');
        a.href = imgs[i].parentNode.href;
        imdiv = document.createElement('div');
        img.src = imgs[i].src;
        img.height = 25;
        img.width = 25;
        imdiv.id = 'summary';
        img.id = 'summaryImg';
        imdiv.onmouseover = function() {div.style.height = "330px";};
        imdiv.onmouseout = function() {div.style.height = "60px";};
        a.appendChild(img);
        imdiv.style.display = 'inline-block';
        imdiv.appendChild(a);
        imgArray.push(imdiv);
        bigDiv = imgs[i].parentNode.parentNode;
    }



    for (i = 0; i < imgArray.length; i++){
        div.appendChild(imgArray[i]);
    }
    var h = document.createElement('H1');
    var text = document.createTextNode('Summary panel.');
    h.appendChild(text);
    h.style.margin = '0px';
    div.insertBefore(h, div.firstChild);
    document.body.insertBefore(div, document.body.firstChild);

}
)
();